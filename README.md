While there are guiding principles for addiction treatment, we also apply programs that are specific to a patients circumstances. MRC incorporates modern, evidence-based practices while taking into account our patients strengths, needs, and preferences.

Website: https://www.memphisrecovery.com/
